#!/bin/bash

# Usage : ./cicd.sh https://gauss-componentotacostmanual-eu.allawnofs.com/remove-0dc1e9b2e9f2d9a437851009a5949a79/component-ota/23/06/14/77b53202e03140dfa3c4728d26f9325b.zip CPH2449_A23

# Set variables
OTA_URL=$1
OTA_NAME=$2

sudo mount /dev/vdb1 workdir
cp payload-dumper-go workdir
cd workdir

# Cleaning up previous files
rm -rf *.zip OTA

# Downloading the new OTA using aria2
aria2c -x 16 $OTA_URL

# Extracting the OTA
mv *.zip OTA.zip
unzip OTA.zip -d OTA
cp payload-dumper-go OTA
cd OTA
./payload-dumper-go payload.bin
cd ..

# Now we have all firmware and system images extracted, create the mountpoints
rm -rf mountpoints && mkdir mountpoints mountpoints/system mountpoints/my_product

# Mount images
sudo mount -o loop -t erofs OTA/extracted_*/system.img mountpoints/system
sudo mount -o loop -t erofs OTA/extracted_*/vendor.img mountpoints/system/vendor
sudo mount -o loop -t erofs OTA/extracted_*/odm.img mountpoints/system/odm
sudo mount -o loop -t erofs OTA/extracted_*/product.img mountpoints/system/product
sudo mount -o loop -t erofs OTA/extracted_*/system_ext.img mountpoints/system/system_ext
sudo mount -o loop -t erofs OTA/extracted_*/my_product.img mountpoints/system/my_product

# Copy to git repo
rm -rf git_repo/system
sudo cp -r mountpoints/system git_repo
sudo chmod -R 777 mountpoints/system

# Umount partitions
sudo umount mountpoints/system/my_product
sudo umount mountpoints/system/system_ext
sudo umount mountpoints/system/product
sudo umount mountpoints/system/odm
sudo umount mountpoints/system/vendor
sudo umount mountpoints/system

# Cleaning up previous files
rm -rf *.zip OTA

# Git lfs
cd git_repo
git lfs install
git lfs track *.so

# Replacing some stock files
# Replace IMS apk with CLO ims
rm system/system_ext/priv-app/ims/ims.apk && cp replace_files/clo_ims.apk system/system_ext/priv-app/ims/ims.apk

# Push files to git
git add system/odm/lib64
git commit -m "$OTA_NAME (1/n) : Push odm/lib64"
git push origin master
git add system/odm
git commit -m "$OTA_NAME (2/n) : Push rest of odm"
git push origin master
git add system/vendor
git commit -m "$OTA_NAME (3/n) : Push vendor"
git push origin master
git add system/system_ext
git commit -m "$OTA_NAME (4/n) : Push system_ext"
git push origin master
git add system/my_product
git commit -m "$OTA_NAME (5/n) : Push my_product"
git push origin master
git add -A
git commit -m "$OTA_NAME (6/n) : Push everything else"
git push origin master

# rm pushed files
rm -rf system
